import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostGameBuildComponent } from './post-game-build.component';

describe('PostGameBuildComponent', () => {
  let component: PostGameBuildComponent;
  let fixture: ComponentFixture<PostGameBuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostGameBuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostGameBuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
