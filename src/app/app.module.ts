import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TeamComponent } from './team/team.component';
import { SummonerComponent } from './summoner/summoner.component';
import { ChampionSelectorComponent } from './champion-selector/champion-selector.component';
import { FiltersComponent } from './champion-selector/filters/filters.component';
import { SearchBarComponent } from './champion-selector/filters/search-bar/search-bar.component';
import { PostGameBuildComponent } from './post-game-build/post-game-build.component';
import { PreGameBuildComponent } from './pre-game-build/pre-game-build.component';
import { SummonerSpellsComponent } from './pre-game-build/summoner-spells/summoner-spells.component';
import { RunesComponent } from './pre-game-build/runes/runes.component';
import { WardsComponent } from './pre-game-build/wards/wards.component';
import { SkinSelectorComponent } from './champion-selector/skin-selector/skin-selector.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamComponent,
    SummonerComponent,
    ChampionSelectorComponent,
    FiltersComponent,
    SearchBarComponent,
    PostGameBuildComponent,
    PreGameBuildComponent,
    SummonerSpellsComponent,
    RunesComponent,
    WardsComponent,
    SkinSelectorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
