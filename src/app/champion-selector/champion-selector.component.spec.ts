import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionSelectorComponent } from './champion-selector.component';

describe('ChampionSelectorComponent', () => {
  let component: ChampionSelectorComponent;
  let fixture: ComponentFixture<ChampionSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChampionSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
