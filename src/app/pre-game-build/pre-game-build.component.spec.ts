import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreGameBuildComponent } from './pre-game-build.component';

describe('PreGameBuildComponent', () => {
  let component: PreGameBuildComponent;
  let fixture: ComponentFixture<PreGameBuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreGameBuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreGameBuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
